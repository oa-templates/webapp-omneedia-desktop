framework: ExtJS
modules:
  - '@oa-modules/core'
  - '@oa-modules/extjs-core'
  - '@oa-modules/omneedia-extjs'
  - '@oa-modules/extjs-core-classic'
packages: 
  - moment
auth: null
controllers:
  - CMain
db:
jobs: null
tests: null
icon:
  file: logo.png
splashscreen:
  background-from: "#ffffff"
  background-color: orange
  file: logo.png
